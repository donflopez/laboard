angular.module('laboard-frontend')
    .directive('navbar', [
        '$rootScope', '$timeout',
        function($root, $timeout) {
            var cancel;

            return {
                restrict: 'A',
                link: function($scope, $element, $attrs) {
                    var offDisconnect = $root.$on('socket.disconnect', function() {
                        if (cancel) {
                            $timeout.cancel(cancel);
                        }

                        cancel = $timeout(
                            function() {
                                $element.addClass('disconnected');
                            },
                            500
                        );
                    });

                    var offReady = $root.$on('socket.ready', function() {
                        if (cancel) {
                            $timeout.cancel(cancel);
                        }

                        cancel = $timeout(
                            function() {
                                $element.removeClass('disconnected');
                            },
                            500
                        );
                    });

                    $scope.$on('$destroy', function() {
                        offDisconnect();
                        offReady();
                    });
                }
            };
        }
    ]);
