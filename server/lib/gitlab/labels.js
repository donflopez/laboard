var q = require('q'),
    labels = module.exports = function labels(client, container) {
        this.client = client;
        this.container = container;
    };

labels.prototype = {
    url: function(namespace, project) {
        return '/projects/' + namespace + '%2F' + project + '/labels';
    },

    all: function(token, namespace, project) {
        var url = this.url(namespace, project),
            timerGet = this.container.get('statsd').createTimer('gitlab.get'),
            timerRoute = this.container.get('statsd').createTimer('gitlab.get.labels');

        return this.client.get(token, url)
            .then(function(response) {
                timerGet.stop();
                timerRoute.stop();

                if (response.statusCode !== 200) {
                    throw response;
                }

                return response.body;
            });
    },

    persist: function(token, namespace, project, label) {
        var url = this.url(namespace, project),
            timerPut = this.container.get('statsd').createTimer('gitlab.put'),
            timerRoute = this.container.get('statsd').createTimer('gitlab.put.labels');

        return this.client.post(token, url, label)
            .then(function(response) {
                timerPut.stop();
                timerRoute.stop();

                if (response.statusCode !== 201) {
                    throw response;
                }

                return response.body;
            });
    }
};
