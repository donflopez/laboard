var container = require('../server/container.js'),
    redis = container.get('redis'),
    q = require('q');

redis.keys('laboard:*:*', function(err, keys) {
    keys.forEach(function(key) {
        var promises = [];

        q.ninvoke(redis, 'hget', key, 'columns')
            .then(function(columns) {
                columns = JSON.parse(columns);

                Object.keys(columns).forEach(function(name) {
                    var column = columns[name];

                    promises.push(q.ninvoke(redis, 'hset', key + ':columns', name, JSON.stringify(column)));
                });

                q.all(promises).then(function() {
                    q.ninvoke(redis, 'hdel', key, 'columns')
                });
            });
    });
});
