var q = require('q');

module.exports = function(router, container) {
    router.post('/login',
        function(req, res) {
            var callback = function (token) {
                    var expire = new Date();
                    expire.setDate(expire.getDate() + 7);

                    res.cookie('access_token', JSON.stringify(token), { expires: expire });
                    res.response.ok(token);
                },
                promise;

            if (req.body.username) {
                promise = container.get('gitlab').login(req.body.username, req.body.password);
            } else {
                promise = container.get('gitlab').auth(req.body.password);
            }

            promise.then(callback).catch(res.error);
        }
    );

    router.authenticated.get('/logout',
        function(req, res) {
            res.clearCookie('access_token');
            res.end();
        }
    );

    router.get('/gitlab*',
        function(req, res) {
            res.redirect(301, container.get('config').gitlab_url + req.params[0]);
        }
    );

    router.get('/login/check',
        function(req, res) {
            if (req.cookies.access_token) {
                q.all([
                    container.get('statsd').createTimer('gitlab.get'),
                    container.get('statsd').createTimer('gitlab.get.user'),
                    container.get('http.client').get(container.get('config').gitlab_url + '/api/v3/user?private_token=' + req.cookies.access_token.private_token)
                ])
                    .spread(function(timerGet, timerRoute, response) {
                        timerGet.stop();
                        timerRoute.stop();

                        return response;
                    })
                    .then(function(response) {
                        var body = JSON.parse(response.body);

                        if (response.statusCode !== 200) {
                            res.error(body, response.statusCode);
                        } else {
                            res.response.ok(body);
                        }
                    })
                    .catch(res.error);
            } else {
                res.error.unauthorized();
            }
        }
    );

    router.get('/login/failed',
        function(req, res) {
            res.error.unauthorized();
        }
    );
};
